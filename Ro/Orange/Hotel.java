package Ro.Orange;

public class Hotel implements MyInterface {
    private int dimension;
    private int height;
    private int weight;
    private String style;

    Hotel(int dimension, int height, int weight, String style) {
        this.dimension = dimension;
        this.height = height;
        this.weight = weight;
        this.style = style;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setDimension(int dimension) {
        this.dimension = dimension;
    }

    public int getDimension(){
        return this.dimension;
    }

    public int getHeight () {
        return this.height;
    }

    public int getWeight () {
        return this.weight;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public int calculateSize() {
        return getDimension()+getHeight()+getWeight();
    }

    public String getStyle () {
        return this.style;
    }

    public String toString () {
        return "The style of this bed is called " + getStyle();
    }
}
